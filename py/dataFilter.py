import sys
import pandas as pd
import os


def execute():
    if len(sys.argv) < 8:
        print("[Usage]: python dataFilter.py <input_csv_file> <output_file_location> <mode (top/range)> <attribute> "
              "<begin> <end> <who (actor/actress)>")
        sys.exit(-1)
    inp_file = sys.argv[1]
    out_file_dir = sys.argv[2]
    mode = sys.argv[3]
    attrib = sys.argv[4]
    begin = int(sys.argv[5])
    end = int(sys.argv[6])
    who = sys.argv[7]
    ip_df = pd.read_csv(inp_file, encoding='latin-1')
    op_df = pd.DataFrame(columns=list(ip_df))
    out_file = ''
    if mode.lower() == 'top':
        op_df = ip_df.nlargest(begin, attrib)
        out_file = os.path.join(out_file_dir, "{0}_{1}_{2}_{3}.csv".format(who, attrib, mode, begin))
    else:
        ip_df.sort_values(by=attrib, inplace=True, ascending=True)
        out_file = os.path.join(out_file_dir, "{0}_{1}_{2}_{3}_{4}.csv".format(who, attrib, mode, begin, end))
        if begin == end:
            condition = ip_df[attrib] > begin
        else:
            condition = ip_df[attrib] > begin
            condition &= ip_df[attrib] < end
        op_df = ip_df[condition]
    op_df.to_csv(out_file, index=False)


if __name__ == '__main__':
    if not sys.version_info >= (3, 0):
        print("Mandatory to use python 3+, run as python3 dataFilter.py")
        sys.exit()
    execute()