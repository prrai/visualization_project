import time
import sys
import urllib2
import csv
import os
from urllib2 import Request, urlopen
from urllib2 import HTTPError


def download_page(url):
    try:
        headers = {
            'User-Agent': "Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.17 (KHTML, like Gecko) Chrome/24.0.1312.27 "
                          "Safari/537.17 "}
        req = urllib2.Request(url, headers=headers)
        response = urllib2.urlopen(req)
        page = response.read()
        return page
    except:
        return "Page Not found"


def _images_get_next_item(s):
    start_line = s.find('rg_di')
    if start_line == -1:
        end_quote = 0
        link = "no_links"
        return link, end_quote
    else:
        start_line = s.find('"class="rg_meta"')
        start_content = s.find('"ou"', start_line + 1)
        end_content = s.find(',"ow"', start_content + 1)
        content_raw = str(s[start_content + 6:end_content - 1])
        return content_raw, end_content


def _images_get_all_items(page):
    items_get = []
    ctr = 0
    while True:
        ctr += 1
        if ctr == 8:
            break
        item, end_content = _images_get_next_item(page)
        if item == "no_links":
            break
        else:
            items_get.append(item)
            time.sleep(0.1)
            page = page[end_content:]
    return items_get


if len(sys.argv) < 2:
    print "Usage: image_scraper.py <path_to_csv>"
    sys.exit(-1)

img_dir = os.path.join(os.getcwd(), 'data', 'img')
if not os.path.exists(img_dir):
    os.makedirs(img_dir)
with open(sys.argv[1], 'r') as csv_file:
    reader = list(csv.reader(csv_file))
    for i in range(0, len(reader)):
        item_id = reader[i][1]
        if not item_id.isdigit():
            continue
        query = reader[i][5]
        search_keyword = [query]
        if 'actress' in sys.argv[1]:
            keywords = [' actress portrait']
        else:
            keywords = [' actor portrait']
        print "id: " + item_id + " query: " + query + " i: " + str(i)
        t0 = time.time()
        i = 0
        items = []
        if os.path.exists(os.path.join(img_dir, item_id) + ".jpg"):
            print "Already fetched, skipping"
            continue
        while i < len(search_keyword):
            iteration = "Item no.: " + str(i + 1) + " -->" + " Item name = " + str(search_keyword[i])
            print (iteration)
            print ("Evaluating...")
            search_keywords = search_keyword[i]
            search = search_keywords.replace(' ', '%20')
            j = 0
            while j < len(keywords):
                pure_keyword = keywords[j].replace(' ', '%20')
                url = 'https://www.google.com/search?q='\
                      + search + pure_keyword + \
                      '&espv=2&biw=1366&bih=667&site=webhp&source=lnms&tbm=isch&sa=' \
                      'X&ei=XosDVaCXD8TasATItgE&ved=0CAcQ_AUoAg'
                raw_html = (download_page(url))
                time.sleep(0.1)
                items += _images_get_all_items(raw_html)
                j += 1
            print ("Total Image Links = " + str(len(items)))
            print ("\n")
            i += 1
            t1 = time.time()
            total_time = t1 - t0
            print("Total time taken: " + str(total_time) + " Seconds")
            print ("Starting Download...")

            pk = 0
            k = 0
            errorCount = 0
            while pk < 1:
                try:
                    req = Request(items[k], headers={"User-Agent": "Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.17 "
                                                                   "(KHTML, like Gecko) Chrome/24.0.1312.27 "
                                                                   "Safari/537.17"})
                    response = urlopen(req)
                    output_file = open(os.path.join(img_dir, item_id) + ".jpg", 'wb')
                    data = response.read()
                    output_file.write(data)
                    response.close()
                    pk += 1
                    print("completed ====> " + str(k + 1))

                    k += 1

                except IOError:

                    errorCount += 1
                    print("IOError on image " + str(k + 1))
                    k += 1

                except HTTPError as e:

                    errorCount += 1
                    print("HTTPError" + str(k))
                    k += 1
        print("\n")
print("All are downloaded")
