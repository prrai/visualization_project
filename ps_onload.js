var svg_width = 1500;
var svg_height = 2000;
var svg_obj = d3.select("svg");
var svg_obj_parallel = d3.select("svg");
var curr_data;
var glob_xScale, glob_yScale;
var data_path = "../data/";
var curr_filter = "all/"
var curr_data_actor = "actors.csv"
var curr_data_actress = "actresses.csv"
var curr_filter_desr = "None"

var last_call = '';
var genre_list = ['Action', 'Adult', 'Adventure', 'Animation', 'Biography', 	'Comedy', 'Commercial', 'Crime', 'Documentary', 'Drama', 'Erotica', 	'Experimental',	'Family', 'Fantasy', 'Film-Noir', 'Game-Show', 'Hardcore', 'History', 'Horror', 'Lifestyle', 'Music', 'Musical', 'Mystery',	'News',	'Reality-TV', 'Romance', 'Sci-Fi', 'Sex', 'Short', 'Sport', 'Talk-Show', 'Thriller', 'War', 'Western'];

var tooltip_info = d3.select("body").append("div")
	    .attr("class", "tooltip")
	    .style("opacity", 0)
	    .style("background", "white");

updateFilterDescription();
updateNavDescription();
updateHelpDescription();



function showHelp() {
	tooltip_info.style("opacity", 0.9);
}

function resetHelpMsg() {
	document.getElementById('Help').innerHTML = 'CLICK TO DISPLAY SELECTED ACTORS';
}

function updateHelpDescription () {
	document.getElementById('Help').innerHTML = 'Help';
}

var in_brush_list = []; 
var in_brush_set = new Set();
var old_brush_set = new Set();
var old_brush_list = [];

function filterData(attr, type, begin, end, desc) {
	curr_filter = attr + '/';
	if (type === "top") {
		curr_data_actor = "actor_" + attr + '_' + type + '_' + begin + '.csv'
		curr_data_actress = "actress_" + attr + '_' + type + '_' + begin + '.csv'
	}
	else {
		curr_data_actor = "actor_" + attr + '_' + type + '_' + begin + '_' + end + '.csv'
		curr_data_actress = "actress_" + attr + '_' + type + '_' + begin + '_' + end + '.csv'
	}
    
    console.log(curr_data_actor);
	curr_filter_desr = attr.toUpperCase();
	curr_filter_desr += ' | ' + desc;
	updateFilterDescription();
	reExecuteLastFunction();

}
function resetNavMsg() {
	document.getElementById('navigate-back').innerHTML = ' MAIN MENU';
}

function resetFiltersMsg() {
	document.getElementById('view-name').innerHTML = ' ClICK TO RESET FILTERS';;
}

function reExecuteLastFunction() {
	console.log("Last call was: " + last_call)
	eval(last_call);
}

function resetFiltersMsg() {
	document.getElementById('view-name').innerHTML = ' ClICK TO RESET FILTERS';;
}

function resetFilters() {
	curr_filter_desr = "None"
	curr_filter = "all/"
	curr_data_actor	= "actors.csv"
	curr_data_actress = "actresses.csv"
	updateFilterDescription();
	reExecuteLastFunction();
}

function updateFilterDescription() {
	document.getElementById('view-name').innerHTML = ' Current: ' + (curr_filter_desr);
}

function updateNavDescription () {
	document.getElementById('navigate-back').innerHTML = ' Home';
}

function createRangeArray(array, bin_width, bin_count) {
	var res = []
	var left = d3.min(array);
	for (var i = 1; i<=bin_count; i++) {
		res.push([left, left+bin_width]);
		left += bin_width;
	}
	return res;
}



function drawMDSEucledianForPs(sampling) {
    last_call = "drawMDSEucledianForPs('"+sampling+"')";
	svg_obj = refreshCanvas(svg_width, svg_height);
    svg_obj_parallel = refreshCanvasForParallel(svg_width, svg_height);
    
    var curr_data_actor_csv = data_path+ 'distance/' + curr_data_actor.substr(0, curr_data_actor.length - 4) + '___euclidean.csv';    
    var curr_data_actress_csv = data_path+ 'distance/' + curr_data_actress.substr(0, curr_data_actress.length - 4) + '___euclidean.csv';
    
	if (sampling === "actors") {
		renderScatterPlot(curr_data_actor_csv, 'actors', false, 'X-axis', 'Y-axis', '2D Scatterplot: Visualization of actors similarity using MDS through euclidean distance');
        
        renderParallelPlot(curr_data_actor);
    }
	else {
		renderScatterPlot(curr_data_actress_csv, 'actresses', false, 'X-axis', 'Y-axis', '2D Scatterplot: Visualization of actors similarity using MDS through euclidean distance');
        renderParallelPlot(curr_data_actress);
    }
}

function drawMDSCorrelationForPs(sampling) {
    
    last_call = "drawMDSCorrelationForPs('"+sampling+"')";
    
    var curr_data_actor_csv = data_path+ 'distance/' + curr_data_actor.substr(0, curr_data_actor.length - 4) + '___correlation.csv';
    var curr_data_actress_csv = data_path + 'distance/' + curr_data_actress.substr(0, curr_data_actress.length - 4) + '___correlation.csv';
  
	svg_obj = refreshCanvas(svg_width, svg_height);
    svg_obj_parallel = refreshCanvasForParallel(svg_width, svg_height);
    
	if (sampling === "actors") {
		renderScatterPlot(curr_data_actor_csv, 'actors', false, 'X-axis', 'Y-axis', '2D Scatterplot: Visualization of actors similarity using MDS through correlation distance');
         renderParallelPlot(curr_data_actor);
    }
	else {
		renderScatterPlot(curr_data_actress_csv, 'actresses', false, 'X-axis', 'Y-axis', '2D Scatterplot: Visualization of actors similarity using MDS visualization through correlation distance');
        renderParallelPlot(curr_data_actress);
    }
}

var xmargin = {top: 80, right: 5, bottom: 40, left: 50},
    xwidth = 700 - xmargin.left - xmargin.right,
    xheight = 500 - xmargin.top - xmargin.bottom;

var brush = d3.svg.brush()
        .x(d3.scale.linear().range([10, xwidth ]))
        .y(d3.scale.linear().range([xheight - 10, 0]))
        .on("brushstart", brushstart)
        .on("brush", brushed)
        .on('brushend', brushend);

var xScale = d3.scale.linear().range([0, xwidth]),
yScale = d3.scale.linear().range([xheight, 0]);


function renderScatterPlot(csv, sampling, connect=false, x_label, y_label, title, pca=false) {
    console.log(csv);
	var margin = xmargin,
    width = xwidth,
    height = xheight;
var isStratified = false;

	var xValue = function(d) { return d.x_data;},
        xScale = d3.scale.linear().range([0, width]),
        xMap = function(d) { return xScale(xValue(d));},
        xAxis = d3.svg.axis().scale(xScale).orient("bottom");

	var yValue = function(d) { return d.y_data;},
	    yScale = d3.scale.linear().range([height, 0]),
        yMap = function(d) { return yScale(yValue(d));},
	    yAxis = d3.svg.axis().scale(yScale).orient("left");



	var g = svg_obj.append("g")
	    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
    
    var tooltip = d3.select("body").append("div")
      .attr("class", "tooltip")
      .style("opacity", 0)
      .style("background", "white");

      var cValue = function(d) { return d.sample;},
      color = d3.scale.category10();

//    console.log(data_path + csv);
	d3.csv(data_path + csv, function(error, data) {

	  data.forEach(function(d) {
      if (d.sample === 'True')
          isStratified = true;
	  		d.x_data = +d.x_data;
    		d.y_data = +d.y_data;
	    	d.z_data = d.z_data;
            d.id_only = d.id_data;
	    	d.id_data = data_path + 'img/' + d.id_data + '.jpg';
            d.votes = +d.votes;
            d.rating = +d.rating;
            d.age = +d.age;
            d.heightcm = +d.heightcm;
            d.gross = +d.gross;
            d.budget = +d.budget;
            d.salary = +d.salary;
            d.academy = +d.academy;
            d.golden = +d.golden;
            
	  });

	  xScale.domain([d3.min(data, xValue)-1, d3.max(data, xValue)+1]);
	  yScale.domain([d3.min(data, yValue)-1, d3.max(data, yValue)+1]);
        
      glob_xScale = xScale;
      glob_yScale = yScale;

	  g.append("g")
	      .attr("class", "x axis")
	      .attr("transform", "translate(0," + height + ")")
	      .call(xAxis)
	    .append("text")
	      .attr("class", "label")
	      .attr("x", width)
	      .attr("y", -6)
	      .style("text-anchor", "end")
	      .style("font-size", 12)
	      .text(x_label);

	  g.append("g")
	      .attr("class", "y axis")
	      .call(yAxis)
	    .append("text")
	      .attr("class", "label")
	      .attr("transform", "rotate(-90)")
	      .attr("y", 6)
	      .attr("dy", ".71em")
	      .style("text-anchor", "end")
	      .style("font-size", 12)
	      .text(y_label);

      g.append("text")
        .attr("x", (width / 2))             
        .attr("y", 0 - (margin.top / 2))
        .attr("text-anchor", "middle")  
        .style("font-size", "16px") 
        .style("text-decoration", "underline")  
//        .text(title);

	  g.selectAll(".dot")
	      .data(data)
	    .enter().append("circle")
	      .attr("class", "dot")
	      .attr("r", 3.0)
	      .attr("cx", xMap)
	      .attr("cy", yMap)
	      .style("fill", function(d, i) { return color(i)})
	      .on("mouseover", function(d) {
	          tooltip.transition()
	               .duration(200)
	               .style("opacity", .9)
	               .style("color",'steelblue')
	               .style("font-size", 50)
	               .style("font-weight", 'bold')
	          tooltip.html("<br/> (" + d.z_data + ")")
	          tooltip.html('<img src=' + d.id_data + ' height=50 width=50'+ '>' + "<br/>" + d.z_data)
	               .style("left", (d3.event.pageX - 50) + "px")
	               .style("top", (d3.event.pageY - 85) + "px");
          
          
	      })
	      .on("mouseout", function(d) {
	          tooltip.transition()
	               .duration(500)
	               .style("opacity", 0);
	      })
         
      });

     svg_obj.on("mousedown", function(d) {
            g.call(brush);
         
         
	  var legend = g.selectAll(".legend")
	      .data(color.domain())
	    .enter().append("g")
	      .attr("class", "legend")
	      .attr("transform", function(d, i) { return "translate(0," + i * 20 + ")"; });

        if (isStratified) {
      legend.append("rect")
        .attr("x", width - 18)
        .attr("width", 18)
        .attr("height", 18)
        .style("fill", "steelblue")
        .style("opacity", function(d, i) {
          if(i === 0) 
            return 1 
          else 
            return 0});

    legend.append("text")
        .attr("x", width - 24)
        .attr("y", 9)
        .attr("dy", ".35em")
        .style("text-anchor", "end")
        .text("Stratified_2000_Samples")
        .style("opacity", function(d, i) {
          if(i === 0) 
            return 1 
          else 
            return 0});
      }

      var line = d3.svg.line()
    			.x(xMap)
    			.y(yMap);
        
    
      if (connect) {
      	if (sampling === 'both') {
      		data_random = data.filter(function(elem) {
	  			return elem.z_data === 'Random_Sampling_Data';
	  		});
      		data_adaptive = data.filter(function(elem) {
	  			return elem.z_data === 'Adaptive_Sampling_Data';
	  		});
	  		g.append("path")
      		.datum(data_random)
			.attr("fill", "none")
			.attr("stroke", function(d) { return color(4);})
			.attr("stroke-linejoin", "round")
			.attr("stroke-linecap", "round")
			.attr("stroke-width", 1.5)
			.attr("d", line);

			g.append("path")
      		.datum(data_adaptive)
			.attr("fill", "none")
			.attr("stroke", function(d) { return color(5);})
			.attr("stroke-linejoin", "round")
			.attr("stroke-linecap", "round")
			.attr("stroke-width", 1.5)
			.attr("d", line);
      	}
      	else {
			g.append("path")
			.datum(data)
			.attr("fill", "none")
			.attr("stroke", function(d) { return color(cValue(d));})
			.attr("stroke-linejoin", "round")
			.attr("stroke-linecap", "round")
			.attr("stroke-width", 1.5)
			.attr("d", line);
      	}
      	if (pca) {
      		g.append("svg:line")
      	.attr("class", 'd3-dp-line')
                    .attr("x1", xScale(d3.min(data, xValue)-1))
                    .attr("y1", yScale(1))
                    .attr("x2", width)
                    .attr("y2", yScale(1))
                    .style("stroke-dasharray", ("3, 3"))
                    .style("stroke-opacity", 0.9)
                    .style("stroke", 'black');

      	}
      	else {
      		g.append("svg:line")
      	.attr("class", 'd3-dp-line')
                    .attr("x1", xScale(d3.min(data, xValue)-1))
                    .attr("y1", yScale(data[2].y_data))
                    .attr("x2", xScale(data[2].x_data))
                    .attr("y2", yScale(data[2].y_data))
                    .style("stroke-dasharray", ("3, 3"))
                    .style("stroke-opacity", 0.9)
                    .style("stroke", 'black');

      	g.append("svg:line")
      	.attr("class", 'd3-dp-line')
                    .attr("x1", xScale(data[2].x_data))
                    .attr("y1", yScale(d3.min(data, yValue)-1))
                    .attr("x2", xScale(data[2].x_data))
                    .attr("y2", yScale(data[2].y_data))
                    .style("stroke-dasharray", ("3, 3"))
                    .style("stroke-opacity", 0.9)
                    .style("stroke", 'black');
      	}
      }	
	}); 
    
    svg_obj.on("mouseover", function(d) {
        
        var toolnames = "";
        
//        console.log("old brush");
//        console.log(old_brush_list);
        
        
        for(var i = 0;i<old_brush_list.length;i++) {
            toolnames = toolnames + "<br\>" + old_brush_list[i];
        }
//        console.log("toolnames");
//        console.log(toolnames);
        
		tooltip_info.transition()
	       .style("opacity", 0)
	       .style("color",'steelblue')
	       .style("font-size", 18 + "px")
	       .style("font-weight", 'bold')
	       .style("border-style", 'solid')
           .style("border-width", 0.5 + "px")
           .style("border-color", 'grey')
	  		tooltip_info.html("<br/> <font color='#ae2727'> <b> Selected Actors </b> : " + old_brush_list.length + "<br/> <font size='2' color='steelblue'>" + toolnames )
           .style("left", 1200 + "px")
           .style("top", 128 + "px");
	});
}

  function brushstart() {
        brush.clear(); 
        old_brush_set.clear();
//        old_brush_list = [];
       
     }
    
    function brushed() {
         var s = brush.extent();
        
         var topleft = s[0];
         var bottomright = s[1];
        
         x0 = topleft[0];
         y0 = topleft[1];
        
         x1 = bottomright[0];
         y1 = bottomright[1];
        
    svg_obj.selectAll("circle").classed("hidden", function(d) {
  
      if (glob_xScale(d.x_data) < xScale(x0) || glob_xScale(d.x_data) > xScale(x1) || 
          glob_yScale(d.y_data) > yScale(y0) || glob_yScale(d.y_data) < yScale(y1)) {
          return true;
      }
        else {
            in_brush_set.add(d);
//            tooltipname = tooltipname + + "<br/> " + d.z_data;
            return false;
        }
    });
        let in_brush_list = Array.from(in_brush_set);   
        
         console.log(in_brush_list.length);
        old_brush_list = [];
         for(i = 0;i<in_brush_list.length;i++) {
             old_brush_list.push(in_brush_list[i].z_data);
         }
        
        console.log(old_brush_list)    ;  
        callbrush(in_brush_list);
    
    }

   
        
     function brushend() {
         
        console.log("brush end");        
         in_brush_list = [];
         in_brush_set.clear();
//         console.log(old_brush_selection);
         emptybrush();
         
         if (brush.empty()) {
             svg_obj.selectAll(".hidden").classed("hidden", false);
//             svg_obj.selectAll(".show_brush").classed("show_brush", true);
             
         }   
     }

var dimensions,
    line,
    axis,
    background,
    foreground,
    foregroundcopy,
    x;

var y = {};

function renderParallelPlot(csv) {
	var diameter = 300, //max size of the bubbles
    color = d3.scale.category20(); //color categor
    
    var margin = {top: 80, right: 5, bottom: 10, left: 0},
        
    width = 700 - margin.left - margin.right,
    height = 500 - margin.top - margin.bottom;
    
    x = d3.scale.ordinal().rangePoints([0, width], 1);
    
    var dragging = {};

    line = d3.svg.line();
    axis = d3.svg.axis().orient("left");

    var g = svg_obj_parallel.append('g')
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
    
    var exclude_list = ['name', 'id', 'dob', 'academy awards nominated', 'golden globes nominated', 'totalawards'];
    exclude_list = exclude_list.concat(genre_list)
    
    d3.csv(data_path + curr_filter + csv, function(error, data) {
		if (error) throw error;

  // Extract the list of dimensions and create a scale for each.
      x.domain(dimensions = d3.keys(data[0]).filter(function(d) {
        return exclude_list.indexOf(d) == -1 && (y[d] = d3.scale.linear()
            .domain(d3.extent(data, function(p) { return +p[d]; }))
            .range([height, 0]));
    }));

    background = g.append("g")
      .attr("class", "background")
    .selectAll("path")
      .data(data)
    .enter().append("path")
      .attr("d", path);

  // Add blue foreground lines for focus.
  foreground = g.append("g")
      .attr("class", "foreground")
    .selectAll("path")
      .data(data)
    .enter().append("path")
      .attr("d", path)
      .style("stroke", function(d) { 
      return color(d.age)
//      return "steelblue";
  });

    foregroundcopy = foreground;
  // Add a group element for each dimension.
  g = g.selectAll(".dimension")
      .data(dimensions)
    .enter().append("g")
      .attr("class", "dimension")
      .attr("transform", function(d) { return "translate(" + x(d) + ")"; })
      .call(d3.behavior.drag()
        .origin(function(d) { return {x: x(d)}; })
            
        .on("dragstart", function(d) {
          dragging[d] = x(d);
          background.attr("visibility", "hidden");
//        console.log("draggin");
        })
            
            
        .on("drag", function(d) {
          dragging[d] = Math.min(width, Math.max(0, d3.event.x));
          foreground.attr("d", path);
          dimensions.sort(function(a, b) { return position(a) - position(b); });
          x.domain(dimensions);
          g.attr("transform", function(d) { return "translate(" + position(d) + ")"; })
//      console.log("in drag");
        })
        .on("dragend", function(d) {
          delete dragging[d];
          transition(d3.select(this)).attr("transform", "translate(" + x(d) + ")");
          transition(foreground).attr("d", path);
          background
              .attr("d", path)
            .transition()
              .delay(500)
              .duration(0)
              .attr("visibility", null);
      
      console.log("in dragend");
        }));

  // Add an axis and title.
  g.append("g")
      .attr("class", "axis")
      .each(function(d) { d3.select(this).call(axis.scale(y[d])); })
    .append("text")
      .style("text-anchor", "middle")
      .attr("y", -9)
      .text(function(d) { return d; });

  // Add and store a brush for each axis.
  g.append("g")
      .attr("class", "brush")
      .each(function(d) {
        d3.select(this).call(y[d].brush = d3.svg.brush().y(y[d]).on("brushstart", brushstart).on("brush", brush));
      })
    .selectAll("rect")
      .attr("x", -8)
      .attr("width", 16);
});

function position(d) {
  var v = dragging[d];
  return v == null ? x(d) : v;
}

function transition(g) {
  return g.transition().duration(500);
}

// Returns the path for a given data point.
function path(d) {
  return line(dimensions.map(function(p) { return [position(p), y[p](d[p])]; }));
}

function brushstart() {
  d3.event.sourceEvent.stopPropagation();
}

// Handles a brush event, toggling the display of foreground lines.
function brush() {
  var actives = dimensions.filter(function(p) { return !y[p].brush.empty(); }),
      extents = actives.map(function(p) { return y[p].brush.extent(); });
    
//    console.log(actives);
//    console.log(extents);
    
  foreground.style("display", function(d) {
    return actives.every(function(p, i) {
      return extents[i][0] <= d[p] && d[p] <= extents[i][1];
    }) ? null : "none";
  })
  .style("stroke", function(d) { return "#FF335F"});
}
    


}


function callbrush(arr) {
//    console.log("in call brush. len = " + arr.length);
    var actives = [];
    var extents = [];
//    console.log(arr);
    
    var votesmax = -1;
    var votesmin = 99999999;
    
    var ratingmax = -1;
    var ratingmin = 99999999;
    
    var agemax = -1;
    var agemin = 99999999;
    
    var heightcmmax = -1;
    var heightcmmin = 99999999;
    
    var grossmax = -1;
    var grossmin = 999999999999;
    
    
    var budgetmax = -1;
    var budgetmin = 99999999;
    
    var salarymax = -1;
    var salarymin = 99999999;
    
    var academymax = -1;
    var academymin = 99999999;
    
    var goldenmax = -1;
    var goldenmin = 99999999;
    
    
    
    for(var i = 0;i<arr.length;i++) {
        
        votesmax = Math.max(votesmax, arr[i].votes);
        votesmin = Math.min(votesmin, arr[i].votes);
        
        ratingmax = Math.max(ratingmax, arr[i].rating);
        ratingmin = Math.min(ratingmin, arr[i].rating);
        
        agemax = Math.max(agemax, arr[i].age);
        agemin = Math.min(agemin, arr[i].age);
        
        heightcmmax = Math.max(heightcmmax, arr[i].heightcm);
        heightcmmin = Math.min(heightcmmin, arr[i].heightcm);
        
        
        grossmax = Math.max(grossmax, arr[i].gross);
        grossmin = Math.min(grossmin, arr[i].gross);
        
        
        budgetmax = Math.max(budgetmax, arr[i].budget);
        budgetmin = Math.min(budgetmin, arr[i].budget);
        
        salarymax = Math.max(salarymax, arr[i].salary);
        salarymin = Math.min(salarymin, arr[i].salary);
        
    }

    var votesarr = []    
    votesarr.push(votesmin);
    votesarr.push(votesmax);
    actives.push("votes");
    extents.push(votesarr);
    
    var ratingsarr = []    
    ratingsarr.push(ratingmin);
    ratingsarr.push(ratingmax);
    actives.push("rating");
    extents.push(ratingsarr);
    
    
    var agearr = []    
    agearr.push(agemin);
    agearr.push(agemax);
    actives.push("age");
    extents.push(agearr);
    
    
    var heightcmarr = []    
    heightcmarr.push(heightcmmin);
    heightcmarr.push(heightcmmax);
    actives.push("heightcm");
    extents.push(heightcmarr);
    
    var grossarr = []    
    grossarr.push(grossmin);
    grossarr.push(grossmax);
    actives.push("gross");
    extents.push(grossarr);
    
    
    var budgetarr = []    
    budgetarr.push(budgetmin);
    budgetarr.push(budgetmax);
    actives.push("budget");
    extents.push(budgetarr);
    
    var salaryarr = []    
    salaryarr.push(salarymin);
    salaryarr.push(salarymax);
    actives.push("salary");
    extents.push(salaryarr);
    
    
  foreground.style("display", function(d) {
           
    return actives.every(function(p, i) {
      return extents[i][0] <= d[p] && d[p] <= extents[i][1];
    }) ? null : "none";
  })
  .style("stroke", function(d) { 
      return "#FF335F"});
}

function emptybrush() {
    
    var color = d3.scale.category20();

//     foreground = svg_obj_parallel
//        .selectAll("path")
//      .style("stroke", function(d) { 
//         return color(d.age)
//     });
}
    
    

function refreshCanvas(width, height) {
	document.getElementsByClassName("svg-container")[0].innerHTML = "";
	return d3.select(".svg-container")
	.append("svg")
	.attr("width", width/2)
	.attr("height", height/2);
}

function refreshCanvasForParallel(width, height) {
	document.getElementsByClassName("svg-container-parallel")[0].innerHTML = "";
	return d3.select(".svg-container-parallel")
	.append("svg")
	.attr("width", width/2)
	.attr("height", height/2);
}