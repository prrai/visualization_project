var data_heights = [];
var data_weights = [];
var data_averages = [];
var data_homeruns = [];
var bin_count = 10;
var bin_width = 0;
var svg_width = 1300;
var svg_height = 1000;
var bar_padding = 5;
var svg_obj = d3.select("svg");
var color_before_over;
var curr_data;
var curr_var;
var curr_render;
var fdg_dist = 20;
var glob_xScale, glob_yScale;
var data_path = "../data/";
var curr_filter = "all/"
var curr_data_actor = "actors.csv"
var curr_data_actress = "actresses.csv"
var curr_filter_desr = "None"
var g_height = 610;
var overall_max = -1
var overall_min = 10000000
var last_call = ''
var genre_list = ['Action', 'Adult', 'Adventure', 'Animation', 'Biography', 	'Comedy', 'Commercial', 'Crime', 'Documentary', 'Drama', 'Erotica', 	'Experimental',	'Family', 'Fantasy', 'Film-Noir', 'Game-Show', 'Hardcore', 'History', 'Horror', 'Lifestyle', 'Music', 'Musical', 'Mystery',	'News',	'Reality-TV', 'Romance', 'Sci-Fi', 'Sex', 'Short', 'Sport', 'Talk-Show', 'Thriller', 'War', 'Western']
var tooltip_info = d3.select("body").append("div")
	    .attr("class", "tooltip")
	    .style("opacity", 0)
	    .style("background", "white");

updateFilterDescription();
updateNavDescription();
updateHelpDescription();

function showHelp() {
	tooltip_info.style("opacity", 0.9);
}

function filterData(attr, type, begin, end, desc) {
	curr_filter = attr + '/';
	if (type === "top") {
		curr_data_actor = "actor_" + attr + '_' + type + '_' + begin + '.csv'
		curr_data_actress = "actress_" + attr + '_' + type + '_' + begin + '.csv'
	}
	else {
		curr_data_actor = "actor_" + attr + '_' + type + '_' + begin + '_' + end + '.csv'
		curr_data_actress = "actress_" + attr + '_' + type + '_' + begin + '_' + end + '.csv'
	}
    
    console.log(curr_data_actor);
	curr_filter_desr = attr.toUpperCase();
	curr_filter_desr += ' | ' + desc;
	updateFilterDescription();
	reExecuteLastFunction();

}
function resetNavMsg() {
	document.getElementById('navigate-back').innerHTML = ' MAIN MENU';
}

function resetFiltersMsg() {
	document.getElementById('view-name').innerHTML = ' ClICK TO RESET FILTERS';;
}

function resetHelpMsg() {
	document.getElementById('Help').innerHTML = 'CLICK FOR INFO ON CURRENT VISUAL';
}

function reExecuteLastFunction() {
	console.log("Last call was: " + last_call)
	eval(last_call);
}

function resetFiltersMsg() {
	document.getElementById('view-name').innerHTML = ' ClICK TO RESET FILTERS';;
}

function resetFilters() {
	curr_filter_desr = "None"
	curr_filter = "all/"
	curr_data_actor	= "actors.csv"
	curr_data_actress = "actresses.csv"
	updateFilterDescription();
	reExecuteLastFunction();
}

function updateFilterDescription() {
	document.getElementById('view-name').innerHTML = ' Current: ' + (curr_filter_desr);
}

function updateNavDescription () {
	document.getElementById('navigate-back').innerHTML = ' Home';
}

function updateHelpDescription () {
	document.getElementById('Help').innerHTML = 'Help';
}

function createRangeArray(array, bin_width, bin_count) {
	var res = []
	var left = d3.min(array);
	for (var i = 1; i<=bin_count; i++) {
		res.push([left, left+bin_width]);
		left += bin_width;
	}
	return res;
}


function gradientColor(pallete_range, data) {
	var red_component = parseInt(data * pallete_range * 0.2);
	var green_component = parseInt(data * pallete_range * 0.60);
	var blue_component = parseInt(data * pallete_range * 0.8);
	return "rgb(" + red_component + "," + green_component + "," + blue_component + ")";
}


function drawDualHistogram(name_array, bin_count) {
	last_call = "drawDualHistogram('" + name_array + "', " + bin_count + ")";
	var male_array = []
	var female_array = []
	var concatenated_histogram_data = new Array(bin_count + 1);
	d3.csv(data_path + curr_filter + curr_data_actor, function(data1) {
		d3.csv(data_path + curr_filter + curr_data_actress, function(data2) {
			if (name_array === 'age') {
				data1.forEach(function(d) {
					if (Number(d[name_array]) > 0 && Number(d[name_array]) < 100)
						male_array.push(Number(d[name_array]));
					});
				data2.forEach(function(d) {
					if (Number(d[name_array]) > 0 && Number(d[name_array]) < 100)
						female_array.push(Number(d[name_array]));
					});
			}
			else {
				data1.forEach(function(d) {
					if (Number(d[name_array]) > 0)
						male_array.push(Number(d[name_array]));
					});
				data2.forEach(function(d) {
					if (Number(d[name_array]) > 0)
						female_array.push(Number(d[name_array]));
					});
			}
			bin_width = getDualBinWidth(male_array, female_array, bin_count);
			var male_histogram_data = fillFrequencyArray(male_array, bin_count, bin_width);
			var female_histogram_data = fillFrequencyArray(female_array, bin_count, bin_width);

			for (iter = 0; iter < male_histogram_data.length; iter++) {
				concatenated_histogram_data[iter] = male_histogram_data[iter] + female_histogram_data[iter];
			}
			renderDualHistogram(male_histogram_data, female_histogram_data, concatenated_histogram_data, male_array, female_array, bin_count, bin_width, name_array);
		});
	});
}

function renderDualHistogram(male_histogram_data, female_histogram_data, concatenated_histogram_data, male_array, female_array, bin_count, bin_width, name_array) {
	svg_obj = refreshCanvas(svg_width, g_height);

	var margin = {top: 20, right: 0, bottom: 30, left: 20};

var g = svg_obj.append("g")
	    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    var tooltip = d3.select("body").append("div")
	    .attr("class", "tooltip")
	    .style("opacity", 0)
	    .style("background", "white");


	    var yValue = function(d) { return d;},
	    yScale = d3.scale.linear().range([g_height - margin.bottom, 0]).domain([0, d3.max(concatenated_histogram_data)]),
	    yMap = function(d) { return yScale(yValue(d));},
	    yAxis = d3.svg.axis().scale(yScale).orient("left").ticks(20);

	    var xValue = function(d) { return d;},
	    xScale = d3.scale.linear().range([0, svg_width]).domain([overall_min, overall_max]),
	    xMap = function(d) { return xScale(xValue(d));},
	    xAxis = d3.svg.axis().scale(xScale).orient("bottom");
	g.append("g")
	      .attr("class", "axis axis--y")
	      .call(yAxis)
	    .append("text")
	      .attr("transform", "rotate(-90)")
	      .attr("y", -30)
	      .attr("dy", "0.71em")
	      .attr("text-anchor", "end")
	      .text("Frequency")
	      .style("font-size", 9);


  	g.append("g")
	      .attr("class", "x axis")
	      .attr("transform", "translate(0," + (g_height - margin.bottom) + ")")
	      .call(xAxis)
	    .append("text")
	      .attr("class", "label")
	      .attr("x", svg_width - 20)
	      .attr("y", 12)
	      .style("text-anchor", "end")
	      .style("font-size", 12)
	      .text(name_array);


	///////////
	var histogram_bar = 
	g.selectAll("bar")
	.data(concatenated_histogram_data)
	.enter()
	.append("rect")
	.attr("x", function(d, i) {
		return i * (svg_width / concatenated_histogram_data.length);
	})
	.attr("width", (svg_width / concatenated_histogram_data.length - bar_padding))
	.attr("y", function(d) {
		return yScale(d);
	})
	.attr("height", function(d) {
		return g_height - yScale(d) - margin.bottom;
	})
	.attr('fill', 'orange');

	histogram_bar.on("mouseover", function(d, i) {
	          tooltip.transition()
	               .duration(200)
	               .style("opacity", .9)
	               .style("color",'steelblue')
	               .style("font-size", 30)
	               .style("font-weight", 'bold')
	          tooltip.html("<br/> (Male: " + Math.round(100 * (male_histogram_data[i] / concatenated_histogram_data[i]))
		        + "%, Female: " + Math.round(100 * (female_histogram_data[i] / concatenated_histogram_data[i])) + "% )")
	               .style("left", (d3.event.pageX - 30) + "px")
	               .style("top", (d3.event.pageY - 65) + "px");
	      })
  histogram_bar.on("mouseout", function(d) {
	          tooltip.transition()
	               .duration(500)
	               .style("opacity", 0)
	           })

	// histogram_bar = applyMouseOverActionsOnBars(concatenated_histogram_data, histogram_bar);
	// histogram_bar = applyMouseOutActionsOnBars(concatenated_histogram_data, histogram_bar);

	///////////


	var histogram_bar = 
	g.selectAll("bar")
	.data(male_histogram_data)
	.enter()
	.append("rect")
	.attr("x", function(d, i) {
		return i * (svg_width / male_histogram_data.length);
	})
	.attr("width", (svg_width / male_histogram_data.length - bar_padding))
	.attr("y", function(d) {
		return yScale(d);
	})
	.attr("height", function(d) {
		return g_height - yScale(d) - margin.bottom;
	})
	.attr('fill', 'steelblue');


	histogram_bar.on("mouseover", function(d, i) {
	          tooltip.transition()
	               .duration(200)
	               .style("opacity", .9)
	               .style("color",'steelblue')
	               .style("font-size", 20)
	               .style("font-weight", 'bold')
	          tooltip.html("<br/> (Male: " + Math.round(100 * (male_histogram_data[i] / concatenated_histogram_data[i]))
		        + "%, Female: " + Math.round(100 * (female_histogram_data[i] / concatenated_histogram_data[i])) + "% )")
	               .style("left", (d3.event.pageX - 50) + "px")
	               .style("top", (d3.event.pageY - 85) + "px");
	      })
  histogram_bar.on("mouseout", function(d) {
	          tooltip.transition()
	               .duration(500)
	               .style("opacity", 0)
	           })

  svg_obj.on("mouseover", function(d) {
		tooltip_info.transition()
			.duration(200)
	       .style("opacity", 0)
	       .style("color",'steelblue')
	       .style("font-size", 18 + "px")
	       .style("font-weight", 'bold')
	       .style("border-style", 'solid')
           .style("border-width", 0.5 + "px")
           .style("border-color", 'grey')
	  		tooltip_info.html("<br/> <font color='#ae2727'> <b> Dual Histogram </b> <br/> <font size='2' color='steelblue'> Each bar compares the relative contribution of actors and actresses in the given range for the current attribute. Hover on a bar to get exact frequency percentage.")
           .style("left", 1200 + "px")
           .style("top", 128 + "px");
	});

}



function getDualBinWidth(male_array, female_array, bin_count) {
	overall_max = Math.max(d3.max(male_array), d3.max(female_array))
	overall_min = Math.min(d3.min(male_array), d3.min(female_array))
	return (overall_max - overall_min) / bin_count;
}

function getBinWidth(array, count) {
	return (d3.max(array) - d3.min(array)) / count;
}

function fillFrequencyArray(array, bin_count, bin_width) {
	var index = 0;
	var frequency_array = new Array(bin_count + 1);
	frequency_array.fill(0);
	for (iter = 0; iter < array.length; iter++) {
		index = Math.floor((array[iter] - d3.min(array)) / bin_width);
		frequency_array[index]++;
	}
	return frequency_array;
}


function refreshCanvas(width, height) {
	tooltip_info.transition()
	               .duration(500)
	               .style("opacity", 0)
	document.getElementsByClassName("svg-container")[0].innerHTML = "";
	return d3.select(".svg-container")
	.append("svg")
	.attr("width", width)
	.attr("height", height);
}