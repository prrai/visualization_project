IMPORTANT:

We have not included the dataset as part of submission, which is mandatory to run the project code correctly.

One can download the data directory and place in root folder (same level as css, img, ..) from the following link to start using the project: 
https://drive.google.com/open?id=0B7DFHsWHXezzaWpfUmhfMkZJM0k