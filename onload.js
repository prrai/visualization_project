var data_heights = [];
var data_weights = [];
var data_averages = [];
var data_homeruns = [];
var bin_count = 10;
var bin_width = 0;
var svg_width = 1300;
var svg_height = 1000;
var bar_padding = 5;
var svg_obj = d3.select("svg");
var color_before_over;
var curr_data;
var curr_var;
var curr_render;
var fdg_dist = 20;
var glob_xScale, glob_yScale;
var data_path = "../data/";
var curr_filter = "all/"
var curr_data_actor = "actors.csv"
var curr_data_actress = "actresses.csv"
var curr_filter_desr = "None"
var g_height = 610;
var overall_max = -1
var overall_min = 10000000
var last_call = ''
var genre_list = ['Action', 'Adult', 'Adventure', 'Animation', 'Biography', 	'Comedy', 'Commercial', 'Crime', 'Documentary', 'Drama', 'Erotica', 	'Experimental',	'Family', 'Fantasy', 'Film-Noir', 'Game-Show', 'Hardcore', 'History', 'Horror', 'Lifestyle', 'Music', 'Musical', 'Mystery',	'News',	'Reality-TV', 'Romance', 'Sci-Fi', 'Sex', 'Short', 'Sport', 'Talk-Show', 'Thriller', 'War', 'Western']
var tooltip_info = d3.select("body").append("div")
	    .attr("class", "tooltip")
	    .style("opacity", 0)
	    .style("background", "white");

updateFilterDescription();
updateNavDescription();
updateHelpDescription();

function showHelp() {
	tooltip_info.style("opacity", 0.9);
}

function filterData(attr, type, begin, end, desc) {
	curr_filter = attr + '/';
	if (type === "top") {
		curr_data_actor = "actor_" + attr + '_' + type + '_' + begin + '.csv'
		curr_data_actress = "actress_" + attr + '_' + type + '_' + begin + '.csv'
	}
	else {
		curr_data_actor = "actor_" + attr + '_' + type + '_' + begin + '_' + end + '.csv'
		curr_data_actress = "actress_" + attr + '_' + type + '_' + begin + '_' + end + '.csv'
	}
    
    console.log(curr_data_actor);
	curr_filter_desr = attr.toUpperCase();
	curr_filter_desr += ' | ' + desc;
	updateFilterDescription();
	reExecuteLastFunction();

}
function resetNavMsg() {
	document.getElementById('navigate-back').innerHTML = ' MAIN MENU';
}

function resetFiltersMsg() {
	document.getElementById('view-name').innerHTML = ' ClICK TO RESET FILTERS';;
}

function resetHelpMsg() {
	document.getElementById('Help').innerHTML = 'CLICK FOR INFO ON CURRENT VISUAL';
}

function reExecuteLastFunction() {
	console.log("Last call was: " + last_call)
	eval(last_call);
}

function resetFiltersMsg() {
	document.getElementById('view-name').innerHTML = ' ClICK TO RESET FILTERS';;
}

function resetFilters() {
	curr_filter_desr = "None"
	curr_filter = "all/"
	curr_data_actor	= "actors.csv"
	curr_data_actress = "actresses.csv"
	updateFilterDescription();
	reExecuteLastFunction();
}

function updateFilterDescription() {
	document.getElementById('view-name').innerHTML = ' Current: ' + (curr_filter_desr);
}

function updateNavDescription () {
	document.getElementById('navigate-back').innerHTML = ' Home';
}

function updateHelpDescription () {
	document.getElementById('Help').innerHTML = 'Help';
}

function createRangeArray(array, bin_width, bin_count) {
	var res = []
	var left = d3.min(array);
	for (var i = 1; i<=bin_count; i++) {
		res.push([left, left+bin_width]);
		left += bin_width;
	}
	return res;
}

function renderBubbleChart(csv) {
	var diameter = 680, //max size of the bubbles
    color = d3.scale.category20b(); //color category

    var margin = {top: 10, right: 10, bottom: 10, left: 250}

var bubble = d3.layout.pack()
    .sort(null)
    .size([diameter, diameter])
    .padding(8.0);

var g = svg_obj.append("g")
	.attr("transform", "translate(" + margin.left + "," + margin.top + ")")
    .attr("class", "bubble")

    	var tooltip = d3.select("body").append("div")
	    .attr("class", "tooltip")
	    .style("opacity", 0)
	    .style("background", "white");

d3.csv(data_path + curr_filter + csv, function(error, data){

    //convert numerical values from strings to numbers
    data = data.map(function(d){ d.value = +d["gross"]; return d; });
    //bubbles needs very specific format, convert data to this.
    var nodes = bubble.nodes({children:data}).filter(function(d) { return !d.children; });
    //setup the chart
    var bubbles = g.append("g")
        .selectAll(".bubble")
        .data(nodes)
        .enter();

    //create the bubbles
    bubbles.append("circle")
        .attr("r", function(d){ return d.r; })
        .attr("cx", function(d){ return d.x; })
        .attr("cy", function(d){ return d.y; })
        .style("fill", function(d) { return color(d.value); });

    //format the text for each bubble
    bubbles.append("text")
        .attr("x", function(d){ return d.x; })
        .attr("y", function(d){ return d.y + 5; })
        .attr("text-anchor", "middle")
        .text(function(d){ return d["name"]; })
        .on("mouseover", function(d) {
	          tooltip.transition()
	               .duration(200)
	               .style("opacity", .9)
	               .style("color",'steelblue')
	               .style("font-size", 20)
	               .style("font-weight", 'bold')
	          tooltip.html('<img src=' + data_path + 'img/' + d.id + '.jpg' + ' height=70 width=70'+ '>' + "<br/>" + d.name)
	               .style("left", (d3.event.pageX - 50) + "px")
	               .style("top", (d3.event.pageY - 85) + "px");
	      })
	      .on("mouseout", function(d) {
	          tooltip.transition()
	               .duration(500)
	               .style("opacity", 0)
	           })
        .style({
            "fill":"black",
            "font-family":"Helvetica Neue, Helvetica, Arial, san-serif",
        })
        .style("font-size", function(d) {
        	return 2 * d.r * 0.1;
        });
        svg_obj.on("mouseover", function(d) {
		tooltip_info.transition()
			.duration(200)
	       .style("opacity", 0)
	       .style("color",'steelblue')
	       .style("font-size", 18 + "px")
	       .style("font-weight", 'bold')
	       .style("border-style", 'solid')
           .style("border-width", 0.5 + "px")
           .style("border-color", 'grey')
	  		tooltip_info.html("<br/> <font color='#ae2727'> <b> Bubble Chart </b> <br/> <font size='2' color='steelblue'> The size of the bubble is proportional to the relative gross earnings for all the  movies done by the actor.")
           .style("left", 1200 + "px")
           .style("top", 128 + "px");
	});
})

}


function renderScatterPlot(csv, sampling, connect=false, x_label, y_label, title, pca=false) {
	var margin = {top: 50, right: 20, bottom: 30, left: 90},
    width = 960 - margin.left - margin.right,
    height = 500 - margin.top - margin.bottom;

    var isStratified = false;

	var xValue = function(d) { return d.x_data;},
        xScale = d3.scale.linear().range([0, width]),
        xMap = function(d) { return xScale(xValue(d));},
        xAxis = d3.svg.axis().scale(xScale).orient("bottom");

	var yValue = function(d) { return d.y_data;},
	    yScale = d3.scale.linear().range([height, 0]),
        yMap = function(d) { return yScale(yValue(d));},
	    yAxis = d3.svg.axis().scale(yScale).orient("left");


      var cValue = function(d) { return d.sample;},
      color = d3.scale.category10();


	var g = svg_obj.append("g")
	    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
    
    var tooltip = d3.select("body").append("div")
	    .attr("class", "tooltip")
	    .style("opacity", 0)
	    .style("background", "white");

	d3.csv(data_path + csv, function(error, data) {

	  data.forEach(function(d) {
	        if (d.sample === 'True')
          		isStratified = true;
	  		d.x_data = +d.x_data;
    		d.y_data = +d.y_data;
	    	d.z_data = d.z_data;
	    	d.id_data = data_path + 'img/' + d.id_data + '.jpg'
	  });

	  xScale.domain([d3.min(data, xValue)-1, d3.max(data, xValue)+1]);
	  yScale.domain([d3.min(data, yValue)-1, d3.max(data, yValue)+1]);
        
      glob_xScale = xScale;
      glob_yScale = yScale;

	  g.append("g")
	      .attr("class", "x axis")
	      .attr("transform", "translate(0," + height + ")")
	      .call(xAxis)
	    .append("text")
	      .attr("class", "label")
	      .attr("x", width)
	      .attr("y", -6)
	      .style("text-anchor", "end")
	      .style("font-size", 12)
	      .text(x_label);

	  g.append("g")
	      .attr("class", "y axis")
	      .call(yAxis)
	    .append("text")
	      .attr("class", "label")
	      .attr("transform", "rotate(-90)")
	      .attr("y", 6)
	      .attr("dy", ".71em")
	      .style("text-anchor", "end")
	      .style("font-size", 12)
	      .text(y_label);

      g.append("text")
        .attr("x", (width / 2))             
        .attr("y", 0 - (margin.top / 2))
        .attr("text-anchor", "middle")  
        .style("font-size", "16px") 
        .style("text-decoration", "underline")  
        .text(title);

	  g.selectAll(".dot")
	      .data(data)
	    .enter().append("circle")
	      .attr("class", "dot")
	      .attr("r", 3.0)
	      .attr("cx", xMap)
	      .attr("cy", yMap)
	      .style("fill", function(d, i) { return color(i)})
	      .on("mouseover", function(d) {
	          tooltip.transition()
	               .duration(200)
	               .style("opacity", .9)
	               .style("color",'steelblue')
	               .style("font-size", 50)
	               .style("font-weight", 'bold')
	          tooltip.html("<br/> (" + d.z_data + ")")
	          tooltip.html('<img src=' + d.id_data + ' height=50 width=50'+ '>' + "<br/>" + d.z_data)
	               .style("left", (d3.event.pageX - 50) + "px")
	               .style("top", (d3.event.pageY - 85) + "px");
          
          
	      })
	      .on("mouseout", function(d) {
	          tooltip.transition()
	               .duration(500)
	               .style("opacity", 0);
	      })
         
      });

     svg_obj.on("mousedown", function(d) {
            g.call(brush);
         
         
	  var legend = g.selectAll(".legend")
	      .data(color.domain())
	    .enter().append("g")
	      .attr("class", "legend")
	      .attr("transform", function(d, i) { return "translate(0," + i * 20 + ")"; });


	      if (isStratified) {
      legend.append("rect")
        .attr("x", width - 18)
        .attr("width", 18)
        .attr("height", 18)
        .style("fill", "steelblue")
        .style("opacity", function(d, i) {
          if(i === 0) 
            return 1 
          else 
            return 0});

    legend.append("text")
        .attr("x", width - 24)
        .attr("y", 9)
        .attr("dy", ".35em")
        .style("text-anchor", "end")
        .text("Stratified_2000_Samples")
        .style("opacity", function(d, i) {
          if(i === 0) 
            return 1 
          else 
            return 0});
    }

      var line = d3.svg.line()
    			.x(xMap)
    			.y(yMap);
        
    
      if (connect) {
      	if (sampling === 'both') {
      		data_random = data.filter(function(elem) {
	  			return elem.z_data === 'Random_Sampling_Data';
	  		});
      		data_adaptive = data.filter(function(elem) {
	  			return elem.z_data === 'Adaptive_Sampling_Data';
	  		});
	  		g.append("path")
      		.datum(data_random)
			.attr("fill", "none")
			.attr("stroke", function(d) { return color(4);})
			.attr("stroke-linejoin", "round")
			.attr("stroke-linecap", "round")
			.attr("stroke-width", 1.5)
			.attr("d", line);

			g.append("path")
      		.datum(data_adaptive)
			.attr("fill", "none")
			.attr("stroke", function(d) { return color(5);})
			.attr("stroke-linejoin", "round")
			.attr("stroke-linecap", "round")
			.attr("stroke-width", 1.5)
			.attr("d", line);
      	}
      	else {
			g.append("path")
			.datum(data)
			.attr("fill", "none")
			.attr("stroke", function(d) { return color(cValue(d));})
			.attr("stroke-linejoin", "round")
			.attr("stroke-linecap", "round")
			.attr("stroke-width", 1.5)
			.attr("d", line);
      	}
      	if (pca) {
      		g.append("svg:line")
      	.attr("class", 'd3-dp-line')
                    .attr("x1", xScale(d3.min(data, xValue)-1))
                    .attr("y1", yScale(1))
                    .attr("x2", width)
                    .attr("y2", yScale(1))
                    .style("stroke-dasharray", ("3, 3"))
                    .style("stroke-opacity", 0.9)
                    .style("stroke", 'black');

      	}
      	else {
      		g.append("svg:line")
      	.attr("class", 'd3-dp-line')
                    .attr("x1", xScale(d3.min(data, xValue)-1))
                    .attr("y1", yScale(data[2].y_data))
                    .attr("x2", xScale(data[2].x_data))
                    .attr("y2", yScale(data[2].y_data))
                    .style("stroke-dasharray", ("3, 3"))
                    .style("stroke-opacity", 0.9)
                    .style("stroke", 'black');

      	g.append("svg:line")
      	.attr("class", 'd3-dp-line')
                    .attr("x1", xScale(data[2].x_data))
                    .attr("y1", yScale(d3.min(data, yValue)-1))
                    .attr("x2", xScale(data[2].x_data))
                    .attr("y2", yScale(data[2].y_data))
                    .style("stroke-dasharray", ("3, 3"))
                    .style("stroke-opacity", 0.9)
                    .style("stroke", 'black');
      	}
      }	
	}); 

	     if(~title.indexOf('euclidean')) {
		     svg_obj.on("mouseover", function(d) {
				tooltip_info.transition()
					.duration(200)
			       .style("opacity", 0)
			       .style("color",'steelblue')
			       .style("font-size", 18 + "px")
			       .style("font-weight", 'bold')
			       .style("border-style", 'solid')
		           .style("border-width", 0.5 + "px")
		           .style("border-color", 'grey')
			  		tooltip_info.html("<br/> <font color='#ae2727'> <b> MDS Euclidean </b> <br/> <font size='2' color='steelblue'> This visualization depicts actor similarity by performing MDS using euclidean distance as metric. Mousedrag on an area to perform brushing to analyse on a set of actors.")
		           .style("left", 1200 + "px")
		           .style("top", 128 + "px");
			});
	 }

	 else if(~title.indexOf('correlation')) {
	 	svg_obj.on("mouseover", function(d) {
			tooltip_info.transition()
				.duration(200)
		       .style("opacity", 0)
		       .style("color",'steelblue')
		       .style("font-size", 18 + "px")
		       .style("font-weight", 'bold')
		       .style("border-style", 'solid')
	           .style("border-width", 0.5 + "px")
	           .style("border-color", 'grey')
		  		tooltip_info.html("<br/> <font color='#ae2727'> <b> MDS correlation </b> <br/> <font size='2' color='steelblue'> This visualization depicts actor similarity by performing MDS using correlation distance as metric. Mousedrag on an area to perform brushing to analyse on a set of actors.")
	           .style("left", 1200 + "px")
	           .style("top", 128 + "px");
		});
	 }
	 else {
	 	svg_obj.on("mouseover", function(d) {
			tooltip_info.transition()
				.duration(200)
		       .style("opacity", 0)
		       .style("color",'steelblue')
		       .style("font-size", 18 + "px")
		       .style("font-weight", 'bold')
		       .style("border-style", 'solid')
	           .style("border-width", 0.5 + "px")
	           .style("border-color", 'grey')
		  		tooltip_info.html("<br/> <font color='#ae2727'> <b> 2D Scatterplot </b> <br/> <font size='2' color='steelblue'> This visualization projects a high dimensional feature vector on a two dimensional plane. Mousedrag on an area to perform brushing to analyse on a set of actors.")
	           .style("left", 1200 + "px")
	           .style("top", 128 + "px");
		});
	 }
}

var xmargin = {top: 50, right: 20, bottom: 30, left: 90},
    xwidth = 960 - xmargin.left - xmargin.right,
    xheight = 500 - xmargin.top - xmargin.bottom;

var brush = d3.svg.brush()
        .x(d3.scale.linear().range([10, xwidth ]))
        .y(d3.scale.linear().range([xheight - 10, 0]))
        .on("brushstart", brushstart)
        .on("brush", brushed)
        .on('brushend', brushend);

var xScale = d3.scale.linear().range([0, xwidth]),
yScale = d3.scale.linear().range([xheight, 0]);

var in_brush_list = []; 
var in_brush_set = new Set();

  function brushstart() {
        brush.clear();
     }
    
    function brushed() {
         var s = brush.extent();
        
         var topleft = s[0];
         var bottomright = s[1];
        
         x0 = topleft[0];
         y0 = topleft[1];
        
         x1 = bottomright[0];
         y1 = bottomright[1];
        
        
      
        
    svg_obj.selectAll("circle").classed("hidden", function(d) {
  
      if (glob_xScale(d.x_data) < xScale(x0) || glob_xScale(d.x_data) > xScale(x1) || 
          glob_yScale(d.y_data) > yScale(y0) || glob_yScale(d.y_data) < yScale(y1)) {
          
          return true;
      }
        else {
            in_brush_set.add(d);
            return false;
        }
    });
        
//        svg_obj.selectAll("circle").classed("show_brush", function(d) {
//      return (( glob_xScale(d.x_data) > xScale(x0) && glob_xScale(d.x_data) < xScale(x1)) || 
//          (glob_yScale(d.y_data) < yScale(y0) && glob_yScale(d.y_data) > yScale(y1)));
//    });
    
    }
        
     function brushend() {
        console.log("brush end");

         let in_brush_list = Array.from(in_brush_set);         
         in_brush_list = [];
         in_brush_set.clear();
         
         
         if (brush.empty()) {
             svg_obj.selectAll(".hidden").classed("hidden", false);
//             svg_obj.selectAll(".show_brush").classed("show_brush", true);
             
         }
         
     }


function drawMDSEucledian(sampling) {
    last_call = "drawMDSEucledian('"+sampling+"')";
	svg_obj = refreshCanvas(svg_width, svg_height);
    
    var curr_data_actor_csv = data_path+ 'distance/' + curr_data_actor.substr(0, curr_data_actor.length - 4) + '___euclidean.csv';    
    var curr_data_actress_csv = data_path+ 'distance/' + curr_data_actress.substr(0, curr_data_actress.length - 4) + '___euclidean.csv';
    
	if (sampling === "actors")
		renderScatterPlot(curr_data_actor_csv, 'actors', false, 'X-axis', 'Y-axis', '2D Scatterplot: Visualization of actors similarity using MDS through euclidean distance');
	else
		renderScatterPlot(curr_data_actress_csv, 'actresses', false, 'X-axis', 'Y-axis', '2D Scatterplot: Visualization of actors similarity using MDS through euclidean distance');
}

function drawMDSCorrelation(sampling) {
    
    last_call = "drawMDSCorrelation('"+sampling+"')";
    
    var curr_data_actor_csv = data_path+ 'distance/' + curr_data_actor.substr(0, curr_data_actor.length - 4) + '___correlation.csv';
    var curr_data_actress_csv = data_path + 'distance/' + curr_data_actress.substr(0, curr_data_actress.length - 4) + '___correlation.csv';
  
	svg_obj = refreshCanvas(svg_width, svg_height);
	if (sampling === "actors")
		renderScatterPlot(curr_data_actor_csv, 'actors', false, 'X-axis', 'Y-axis', '2D Scatterplot: Visualization of actors similarity using MDS through correlation distance');
	else
		renderScatterPlot(curr_data_actress_csv, 'actresses', false, 'X-axis', 'Y-axis', '2D Scatterplot: Visualization of actors similarity using MDS visualization through correlation distance');
}

function drawGrossBubble(sampling) {
	last_call = "drawGrossBubble('"+sampling+"')";
	svg_obj = refreshCanvas(svg_width, svg_height);
	if (sampling === "actors")
		renderBubbleChart(curr_data_actor );
	else
		renderBubbleChart(curr_data_actress);
}

function gradientColor(pallete_range, data) {
	var red_component = parseInt(data * pallete_range * 0.2);
	var green_component = parseInt(data * pallete_range * 0.60);
	var blue_component = parseInt(data * pallete_range * 0.8);
	return "rgb(" + red_component + "," + green_component + "," + blue_component + ")";
}


function drawDualHistogram(name_array, bin_count) {
	last_call = "drawDualHistogram('" + name_array + "', " + bin_count + ")";
	var male_array = []
	var female_array = []
	var concatenated_histogram_data = new Array(bin_count + 1);
	d3.csv(data_path + curr_filter + curr_data_actor, function(data1) {
		d3.csv(data_path + curr_filter + curr_data_actress, function(data2) {
			if (name_array === 'age') {
				data1.forEach(function(d) {
					if (Number(d[name_array]) > 0 && Number(d[name_array]) < 100)
						male_array.push(Number(d[name_array]));
					});
				data2.forEach(function(d) {
					if (Number(d[name_array]) > 0 && Number(d[name_array]) < 100)
						female_array.push(Number(d[name_array]));
					});
			}
			else {
				data1.forEach(function(d) {
					if (Number(d[name_array]) > 0)
						male_array.push(Number(d[name_array]));
					});
				data2.forEach(function(d) {
					if (Number(d[name_array]) > 0)
						female_array.push(Number(d[name_array]));
					});
			}
			bin_width = getDualBinWidth(male_array, female_array, bin_count);
			var male_histogram_data = fillFrequencyArray(male_array, bin_count, bin_width);
			var female_histogram_data = fillFrequencyArray(female_array, bin_count, bin_width);

			for (iter = 0; iter < male_histogram_data.length; iter++) {
				concatenated_histogram_data[iter] = male_histogram_data[iter] + female_histogram_data[iter];
			}
			renderDualHistogram(male_histogram_data, female_histogram_data, concatenated_histogram_data, male_array, female_array, bin_count, bin_width, name_array);
		});
	});
}

function renderDualHistogram(male_histogram_data, female_histogram_data, concatenated_histogram_data, male_array, female_array, bin_count, bin_width, name_array) {
	svg_obj = refreshCanvas(svg_width, g_height);

	var margin = {top: 20, right: 0, bottom: 30, left: 20};

var g = svg_obj.append("g")
	    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    var tooltip = d3.select("body").append("div")
	    .attr("class", "tooltip")
	    .style("opacity", 0)
	    .style("background", "white");


	    var yValue = function(d) { return d;},
	    yScale = d3.scale.linear().range([g_height - margin.bottom, 0]).domain([0, d3.max(concatenated_histogram_data)]),
	    yMap = function(d) { return yScale(yValue(d));},
	    yAxis = d3.svg.axis().scale(yScale).orient("left").ticks(20);

	    var xValue = function(d) { return d;},
	    xScale = d3.scale.linear().range([0, svg_width]).domain([overall_min, overall_max]),
	    xMap = function(d) { return xScale(xValue(d));},
	    xAxis = d3.svg.axis().scale(xScale).orient("bottom");
	g.append("g")
	      .attr("class", "axis axis--y")
	      .call(yAxis)
	    .append("text")
	      .attr("transform", "rotate(-90)")
	      .attr("y", -30)
	      .attr("dy", "0.71em")
	      .attr("text-anchor", "end")
	      .text("Frequency")
	      .style("font-size", 9);


  	g.append("g")
	      .attr("class", "x axis")
	      .attr("transform", "translate(0," + (g_height - margin.bottom) + ")")
	      .call(xAxis)
	    .append("text")
	      .attr("class", "label")
	      .attr("x", svg_width - 20)
	      .attr("y", 12)
	      .style("text-anchor", "end")
	      .style("font-size", 12)
	      .text(name_array);


	///////////
	var histogram_bar = 
	g.selectAll("bar")
	.data(concatenated_histogram_data)
	.enter()
	.append("rect")
	.attr("x", function(d, i) {
		return i * (svg_width / concatenated_histogram_data.length);
	})
	.attr("width", (svg_width / concatenated_histogram_data.length - bar_padding))
	.attr("y", function(d) {
		return yScale(d);
	})
	.attr("height", function(d) {
		return g_height - yScale(d) - margin.bottom;
	})
	.attr('fill', 'orange');

	histogram_bar.on("mouseover", function(d, i) {
	          tooltip.transition()
	               .duration(200)
	               .style("opacity", .9)
	               .style("color",'steelblue')
	               .style("font-size", 30)
	               .style("font-weight", 'bold')
	          tooltip.html("<br/> (Male: " + Math.round(100 * (male_histogram_data[i] / concatenated_histogram_data[i]))
		        + "%, Female: " + Math.round(100 * (female_histogram_data[i] / concatenated_histogram_data[i])) + "% )")
	               .style("left", (d3.event.pageX - 30) + "px")
	               .style("top", (d3.event.pageY - 65) + "px");
	      })
  histogram_bar.on("mouseout", function(d) {
	          tooltip.transition()
	               .duration(500)
	               .style("opacity", 0)
	           })

	// histogram_bar = applyMouseOverActionsOnBars(concatenated_histogram_data, histogram_bar);
	// histogram_bar = applyMouseOutActionsOnBars(concatenated_histogram_data, histogram_bar);

	///////////


	var histogram_bar = 
	g.selectAll("bar")
	.data(male_histogram_data)
	.enter()
	.append("rect")
	.attr("x", function(d, i) {
		return i * (svg_width / male_histogram_data.length);
	})
	.attr("width", (svg_width / male_histogram_data.length - bar_padding))
	.attr("y", function(d) {
		return yScale(d);
	})
	.attr("height", function(d) {
		return g_height - yScale(d) - margin.bottom;
	})
	.attr('fill', 'steelblue');


	histogram_bar.on("mouseover", function(d, i) {
	          tooltip.transition()
	               .duration(200)
	               .style("opacity", .9)
	               .style("color",'steelblue')
	               .style("font-size", 20)
	               .style("font-weight", 'bold')
	          tooltip.html("<br/> (Male: " + Math.round(100 * (male_histogram_data[i] / concatenated_histogram_data[i]))
		        + "%, Female: " + Math.round(100 * (female_histogram_data[i] / concatenated_histogram_data[i])) + "% )")
	               .style("left", (d3.event.pageX - 50) + "px")
	               .style("top", (d3.event.pageY - 85) + "px");
	      })
  histogram_bar.on("mouseout", function(d) {
	          tooltip.transition()
	               .duration(500)
	               .style("opacity", 0)
	           })

  svg_obj.on("mouseover", function(d) {
		tooltip_info.transition()
			.duration(200)
	       .style("opacity", 0)
	       .style("color",'steelblue')
	       .style("font-size", 18 + "px")
	       .style("font-weight", 'bold')
	       .style("border-style", 'solid')
           .style("border-width", 0.5 + "px")
           .style("border-color", 'grey')
	  		tooltip_info.html("<br/> <font color='#ae2727'> <b> Dual Histogram </b> <br/> <font size='2' color='steelblue'> Each bar compares the relative contribution of actors and actresses in the given range for the current attribute. Hover on a bar to get exact frequency percentage.")
           .style("left", 1200 + "px")
           .style("top", 128 + "px");
	});

}



function getDualBinWidth(male_array, female_array, bin_count) {
	overall_max = Math.max(d3.max(male_array), d3.max(female_array))
	overall_min = Math.min(d3.min(male_array), d3.min(female_array))
	return (overall_max - overall_min) / bin_count;
}

function drawHistogram(sampling, name_array, bin_count) {
	last_call = "drawHistogram('" + sampling + "', '" + name_array + "', " + bin_count + ")";
	var array = []
	if (sampling === "actors")
		csv = curr_data_actor
	else
		csv = curr_data_actress
	d3.csv(data_path + curr_filter + csv, function(data) {
		data.forEach(function(d) {
			if (name_array === 'age') {
				if (Number(d[name_array]) <= 100)
					array.push(Number(d[name_array]));
			}
			else {
					if (Number(d[name_array]) > 0)
						array.push(Number(d[name_array]));
			}
		});
		bin_width = getBinWidth(array, bin_count);
		var histogram_data = fillFrequencyArray(array, bin_count, bin_width);
		var range_array = createRangeArray(array, bin_width, bin_count);
		renderHistogram(histogram_data, array, bin_count, bin_width, name_array, sampling, range_array);
	});
}

function drawPieChart(sampling, name_array, bin_count) {
	last_call = "drawPieChart('" + sampling + "', '" + name_array + "', " + bin_count + ')';
	var array = []
	if (sampling === "actors")
		csv = curr_data_actor
	else
		csv = curr_data_actress
	d3.csv(data_path + curr_filter + csv, function(data) {
		data.forEach(function(d) {
			if (name_array === 'age') {
				if (Number(d[name_array]) <= 100)
					array.push(Number(d[name_array]));
			}
			else {
					if (Number(d[name_array]) > 0)
						array.push(Number(d[name_array]));
			}
		});
		bin_width = getBinWidth(array, bin_count);
		var histogram_data = fillFrequencyArray(array, bin_count, bin_width);
		var range_array = createRangeArray(array, bin_width, bin_count);
		renderPieChart(histogram_data, array, sampling, name_array, bin_count, range_array);
	});


}

function renderPieChart(pie_chart_data, array, sampling, name_array, bin_count, range_array) {
	svg_obj = refreshCanvas(svg_width, g_height);
	radius = Math.min(svg_width, g_height) / 2;

	var tooltip = d3.select("body").append("div")
	    .attr("class", "tooltip")
	    .style("opacity", 0)
	    .style("background", "white");

	var arc = d3.svg.arc()
	.outerRadius(radius - 10)
	.innerRadius(0);

	var labelArc = d3.svg.arc()
	.outerRadius(radius - 30)
	.innerRadius(radius - 30);

	var pie = d3.layout.pie()
	.sort(null)
	.value(function(d) { return d; });

	var g = svg_obj.append("g")
	.attr("transform", "translate(" + svg_width / 2 + "," + g_height / 2 + ")");

	g = g.selectAll(".arc")
	.data(pie(pie_chart_data))
	.enter().append("g")
	.attr("class", "arc");

	g.append("path")
	.attr("d", arc)
	.attr("stroke", "white")
	.attr("stroke-width", 1)
	.attr('fill', function(d,i) {
		var pallete_range = 255 / d3.max(pie_chart_data);
		return gradientColor(pallete_range, d.value);
	});


	g.append("text")
	.attr("transform", function(d) { return "translate(" + labelArc.centroid(d) + ")"; })
	.attr("dy", ".35em")
	.text(function(d) { 
		return d.value; 
	})
	.style("opacity", 0)
	.attr("font-family", "sans-serif")
	.attr("font-size", 12)
	.attr("fill", "black")

	g.on("mouseover", function(d,i) {
		var curr_path = d3.selectAll("path")
		.select(function(d, ind) {
			if (ind == i)
				return this;
			return null;})
		color_before_over = curr_path.attr("fill");
		curr_path.attr("fill", "lime");
		curr_path
		.transition().duration(200).delay(function(d, i) { return 50; })
		.attr("d", d3.svg.arc().outerRadius(radius).innerRadius(0));
		curr_path.attr("stroke-width", 2);
		d3.selectAll("text")
		.select(function(d, ind) {
			if (ind == i)
				return this;
			return null;})
		.style("opacity", 100)
		if (range_array[i].length > 1) {
			tooltip.transition()
	               .duration(200)
	               .style("opacity", .9)
	               .style("color",'steelblue')
	               .style("font-size", 15 + "px")
	               .style("font-weight", 'bold')
	          tooltip.html("<br/> Subject: " + sampling + "<br/>" + "Attribute: " + name_array + "<br/>" + "Range: (" + parseFloat(range_array[i][0]).toFixed(2) + ', ' + parseFloat(range_array[i][1]).toFixed(2) + ')' + "<br/>" + '<font color="red">Frequency: ' + pie_chart_data[i] + "<br/>" + "Percentage: " + parseFloat(100 * (pie_chart_data[i] / array.length)).toFixed(2) + '%')
	               .style("left", 1100 + "px")
	               .style("top", 128 + "px");
		}
	});
	g.on("mouseout", function(d,i) {
		var curr_path = d3.selectAll("path")
		.select(function(d, ind) {
			if (ind == i)
				return this;
			return null;})
		curr_path.attr("fill", color_before_over)
		curr_path
		.transition().duration(200).delay(function(d, i) { return 50; })
		.attr("d", d3.svg.arc().outerRadius(radius - 10).innerRadius(0));
		curr_path.attr("stroke-width", 1);
		d3.selectAll("text")
		.select(function(d, ind) {
			if (ind == i)
				return this;
			return null;})
		.style("opacity", 0)
		tooltip.transition()
	           .duration(500)
	           .style("opacity", 0);
	});
	g.on("click", function(d) {
		tooltip.transition()
	           .duration(500)
	           .style("opacity", 0);
		drawHistogram(sampling, name_array, bin_count);
	});

	svg_obj.on("mouseover", function(d) {
		tooltip_info.transition()
			.duration(200)
	       .style("opacity", 0)
	       .style("color",'steelblue')
	       .style("font-size", 18 + "px")
	       .style("font-weight", 'bold')
	       .style("border-style", 'solid')
           .style("border-width", 0.5 + "px")
           .style("border-color", 'grey')
	  		tooltip_info.html("<br/> <font color='#ae2727'> <b> Pie Chart </b> <br/> <font size='2' color='steelblue'> The sector of pie chart indicates the percentage contribution of the subject for the concerned attribute. Hover on each sector for details.")
           .style("left", 1200 + "px")
           .style("top", 128 + "px");
   });

}

function getBinWidth(array, count) {
	return (d3.max(array) - d3.min(array)) / count;
}

function fillFrequencyArray(array, bin_count, bin_width) {
	var index = 0;
	var frequency_array = new Array(bin_count + 1);
	frequency_array.fill(0);
	for (iter = 0; iter < array.length; iter++) {
		index = Math.floor((array[iter] - d3.min(array)) / bin_width);
		frequency_array[index]++;
	}
	return frequency_array;
}

function renderHistogram(histogram_data, array, bin_count, bin_width, name_array, sampling, range_array) {

	svg_obj = refreshCanvas(svg_width, g_height);
		var tooltip = d3.select("body").append("div")
	    .attr("class", "tooltip")
	    .style("opacity", 0)
	    .style("background", "white");

	var margin = {top: 20, right: 0, bottom: 30, left: 20};

var g = svg_obj.append("g")
	    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

		var yValue = function(d) { return d;},
	    yScale = d3.scale.linear().range([g_height - margin.bottom, 0]).domain([0, d3.max(histogram_data)]),
	    yMap = function(d) { return yScale(yValue(d));},
	    yAxis = d3.svg.axis().scale(yScale).orient("left").ticks(20);

	    var xValue = function(d) { return d;},
	    xScale = d3.scale.linear().range([0, svg_width]).domain([d3.min(array), d3.max(array)]),
	    xMap = function(d) { return xScale(xValue(d));},
	    xAxis = d3.svg.axis().scale(xScale).orient("bottom");
	g.append("g")
	      .attr("class", "axis axis--y")
	      .call(yAxis)
	    .append("text")
	      .attr("transform", "rotate(-90)")
	      .attr("y", 6)
	      .attr("dy", "0.71em")
	      .attr("text-anchor", "end")
	      .text("Frequency");


  	g.append("g")
	      .attr("class", "x axis")
	      .attr("transform", "translate(0," + (g_height - margin.bottom) + ")")
	      .call(xAxis)
	    .append("text")
	      .attr("class", "label")
	      .attr("x", svg_width - 20)
	      .attr("y", 12)
	      .style("text-anchor", "end")
	      .style("font-size", 12)
	      .text(name_array);

	var histogram_bar = 
	g.selectAll("bar")
	.data(histogram_data)
	.enter()
	.append("rect")
	.attr("x", function(d, i) {
		return i * (svg_width / histogram_data.length);
	})
	.attr("width", (svg_width / histogram_data.length - bar_padding))
	.attr("y", function(d) {
		return yScale(d);
	})
	.attr("height", function(d) {
		return g_height - yScale(d) - margin.bottom;
	})
	.attr('fill', function(d,i) {
		var pallete_range = 255/d3.max(histogram_data);
		return gradientColor(pallete_range, d);
	});

	var text = g.append('g').attr("class", "text-data")
	.selectAll("text")
	.data(histogram_data)
	.enter()
	.append("text")
	.text(function(d) {
		return d;
	})
	.attr("text-anchor", "middle")
	.attr("x", function(d, i) {
		var width_ratio = svg_width / histogram_data.length;
		return (i * width_ratio) + (width_ratio - bar_padding) / 2;
	})
	.attr("y", function(d) {
		return yScale(d) - 20;
	})
	.attr("font-family", "sans-serif")
	.attr("font-size", 9)
	.attr("fill", "black")
	.style("font-weight", 'bold')
	.style("opacity",0);
	histogram_bar = applyMouseOverActionsOnBars(histogram_data, histogram_bar, sampling, range_array, name_array, tooltip);
		histogram_bar = applyMouseOutActionsOnBars(histogram_data, histogram_bar, tooltip);
		histogram_bar.on("click",function(){
			tooltip.transition()
		           .duration(500)
		           .style("opacity", 0);
			drawPieChart(sampling, name_array, bin_count, tooltip);
		});

		svg_obj.on("mouseover", function(d) {
		tooltip_info.transition()
			.duration(200)
	       .style("opacity", 0)
	       .style("color",'steelblue')
	       .style("font-size", 18 + "px")
	       .style("font-weight", 'bold')
	       .style("border-style", 'solid')
           .style("border-width", 0.5 + "px")
           .style("border-color", 'grey')
	  		tooltip_info.html("<br/> <font color='#ae2727'> <b> Bar Chart </b> <br/> <font size='2' color='steelblue'> The height of a bar indicates the frequency of data point in the given range. Hover on the bars to get details.")
           .style("left", 1200 + "px")
           .style("top", 128 + "px");
	});
}


function applyMouseOverActionsOnBars(histogram_data, histogram_bar, sampling, range_array, name_array, tooltip) {
	var histogram_bar = histogram_bar.on("mouseover", function(d,i) {
		color_before_over = d3.select(this).attr('fill');
		var width_ratio = svg_width / histogram_data.length;
		d3.select(this)
		.attr("y",d3.select(this).attr("y") - 15)
		.attr("height",parseInt(d3.select(this).attr("height")) + 15)
		.attr("x",i * width_ratio - 5)
		.attr("width", (width_ratio - bar_padding + 10))
		.attr('fill', "lime");
		d3.select('.text-data').selectAll("text")
		.select(function(d, ind) {
			if (ind == i)
				return this;
			return null;})
		.style("opacity",100)
		tooltip.transition()
	               .duration(200)
	               .style("opacity", .9)
	               .style("color",'steelblue')
	               .style("font-size", 15 + "px")
	               .style("font-weight", 'bold')
	          tooltip.html("<br/> Subject: " + sampling + "<br/>" + "Attribute: " + name_array + "<br/>" + "Range: (" + parseFloat(range_array[i][0]).toFixed(2) + ', ' + parseFloat(range_array[i][1]).toFixed(2) + ')' + "<br/>" + '<font color="red">Frequency: ' + histogram_data[i])
	               .style("left", 1100 + "px")
	               .style("top", 128 + "px");

	})
	return histogram_bar;
}

function applyMouseOutActionsOnBars(histogram_data, histogram_bar, tooltip) {
	var histogram_bar = histogram_bar.on("mouseout", function(d,i) {
		var width_ratio = svg_width / histogram_data.length;
		d3.select(this)
		.attr("width", (width_ratio - bar_padding))
		.attr("y",parseInt(d3.select(this).attr("y")) + 15)
		.attr("x",i * width_ratio)
		.attr("height",parseInt(d3.select(this).attr("height")) - 15)
		.attr('fill', color_before_over)
		d3.select('.text-data').selectAll("text")
		.select(function(d, ind) {
			if (ind == i)
				return this;
			return null;})
		.style("opacity",0)
		tooltip.transition()
	           .duration(500)
	           .style("opacity", 0);
	})
		
	return histogram_bar;
}


function drawParallelPlot(sampling) {
	last_call = "drawParallelPlot('"+sampling+"')";
	svg_obj = refreshCanvas(svg_width, svg_height);
	if (sampling === "Actors")
		renderParallelPlot(curr_data_actor);
	else
		renderParallelPlot(curr_data_actress);
}


function renderParallelPlot(csv) {
	var diameter = 800, //max size of the bubbles
    color = d3.scale.category20(); //color categor
    
    var margin = {top: 30, right: 10, bottom: 10, left: 10},
    width = 1250 - margin.left - margin.right,
    height = 650 - margin.top - margin.bottom;
    
    var x = d3.scale.ordinal().rangePoints([0, width], 1),
    y = {},
    dragging = {};

    var line = d3.svg.line(),
        axis = d3.svg.axis().orient("left"),
        background,
        foreground;

    var g = svg_obj.append('g')
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
    
    var exclude_list = ['name', 'id', 'dob', 'academy awards nominated', 'golden globes nominated', 'totalawards'];
    exclude_list = exclude_list.concat(genre_list)
    
    d3.csv(data_path + curr_filter + csv, function(error, data) {
		if (error) throw error;

  // Extract the list of dimensions and create a scale for each.
      x.domain(dimensions = d3.keys(data[0]).filter(function(d) {
        return exclude_list.indexOf(d) == -1 && (y[d] = d3.scale.linear()
            .domain(d3.extent(data, function(p) { return +p[d]; }))
            .range([height, 0]));
    }));

    background = g.append("g")
      .attr("class", "background")
    .selectAll("path")
      .data(data)
    .enter().append("path")
      .attr("d", path);

  // Add blue foreground lines for focus.
  foreground = g.append("g")
      .attr("class", "foreground")
    .selectAll("path")
      .data(data)
    .enter().append("path")
      .attr("d", path)
      .style("stroke", function(d) { return color(d.age)});

  // Add a group element for each dimension.
  g = g.selectAll(".dimension")
      .data(dimensions)
    .enter().append("g")
      .attr("class", "dimension")
      .attr("transform", function(d) { return "translate(" + x(d) + ")"; })
      .call(d3.behavior.drag()
        .origin(function(d) { return {x: x(d)}; })
        .on("dragstart", function(d) {
          dragging[d] = x(d);
          background.attr("visibility", "hidden");
        })
        .on("drag", function(d) {
          dragging[d] = Math.min(width, Math.max(0, d3.event.x));
          foreground.attr("d", path);
          dimensions.sort(function(a, b) { return position(a) - position(b); });
          x.domain(dimensions);
          g.attr("transform", function(d) { return "translate(" + position(d) + ")"; })
        })
        .on("dragend", function(d) {
          delete dragging[d];
          transition(d3.select(this)).attr("transform", "translate(" + x(d) + ")");
          transition(foreground).attr("d", path);
          background
              .attr("d", path)
            .transition()
              .delay(500)
              .duration(0)
              .attr("visibility", null);
        }));

  // Add an axis and title.
  g.append("g")
      .attr("class", "axis")
      .each(function(d) { d3.select(this).call(axis.scale(y[d])); })
    .append("text")
      .style("text-anchor", "middle")
      .attr("y", -9)
      .text(function(d) { return d; });

  // Add and store a brush for each axis.
  g.append("g")
      .attr("class", "brush")
      .each(function(d) {
        d3.select(this).call(y[d].brush = d3.svg.brush().y(y[d]).on("brushstart", brushstart).on("brush", brush));
      })
    .selectAll("rect")
      .attr("x", -8)
      .attr("width", 16);
});

function position(d) {
  var v = dragging[d];
  return v == null ? x(d) : v;
}

function transition(g) {
  return g.transition().duration(500);
}

// Returns the path for a given data point.
function path(d) {
  return line(dimensions.map(function(p) { return [position(p), y[p](d[p])]; }));
}

function brushstart() {
  d3.event.sourceEvent.stopPropagation();
}

// Handles a brush event, toggling the display of foreground lines.
function brush() {
  var actives = dimensions.filter(function(p) { return !y[p].brush.empty(); }),
      extents = actives.map(function(p) { return y[p].brush.extent(); });
  foreground.style("display", function(d) {
    return actives.every(function(p, i) {
      return extents[i][0] <= d[p] && d[p] <= extents[i][1];
    }) ? null : "none";
  })
  .style("stroke", function(d) { return "#FF335F"});
}

svg_obj.on("mouseover", function(d) {
		tooltip_info.transition()
			.duration(200)
	       .style("opacity", 0)
	       .style("color",'steelblue')
	       .style("font-size", 18 + "px")
	       .style("font-weight", 'bold')
	       .style("border-style", 'solid')
           .style("border-width", 0.5 + "px")
           .style("border-color", 'grey')
	  		tooltip_info.html("<br/> <font color='#ae2727'> <b> Parallel Plot </b> <br/> <font size='2' color='steelblue'> This visualization depicts the correlation of data points across various attributes. Each coloured line corresponds to a subject actor. Hover on any attribute and select mouse drag to exercise brushing.")
           .style("left", 1200 + "px")
           .style("top", 128 + "px");
	});

}


function refreshCanvas(width, height) {
	tooltip_info.transition()
	               .duration(500)
	               .style("opacity", 0)
	document.getElementsByClassName("svg-container")[0].innerHTML = "";
	return d3.select(".svg-container")
	.append("svg")
	.attr("width", width)
	.attr("height", height);
}